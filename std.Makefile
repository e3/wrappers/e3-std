where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



APP:=stdApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src


USR_INCLUDES += -I$(where_am_I)$(APPSRC)

# USR_CFLAGS   += -Wno-unused-variable
# USR_CFLAGS   += -Wno-unused-function
# USR_CFLAGS   += -Wno-unused-but-set-variable
# USR_CPPFLAGS += -Wno-unused-variable
# USR_CPPFLAGS += -Wno-unused-function
# USR_CPPFLAGS += -Wno-unused-but-set-variable

USR_CPPFLAGS += -DUSE_TYPED_RSET

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.req)

DBDINC_SRCS += $(APPSRC)/epidRecord.c
DBDINC_SRCS += $(APPSRC)/timestampRecord.c
DBDINC_SRCS += $(APPSRC)/throttleRecord.c


DBDINC_DBDS = $(subst .c,.dbd,   $(DBDINC_SRCS:$(APPSRC)/%=%))
DBDINC_HDRS = $(subst .c,.h,     $(DBDINC_SRCS:$(APPSRC)/%=%))
DBDINC_DEPS = $(subst .c,$(DEP), $(DBDINC_SRCS:$(APPSRC)/%=%))


HEADERS += $(DBDINC_HDRS)


SOURCES += $(APPSRC)/devEpidSoft.c
SOURCES += $(APPSRC)/devEpidSoftCallback.c
SOURCES += $(APPSRC)/devEpidFast.c
SOURCES += $(APPSRC)/devTimeOfDay.c 
SOURCES += $(APPSRC)/pvHistory.c
SOURCES += $(APPSRC)/femto.st
SOURCES += $(APPSRC)/delayDo.st
SOURCES += $(APPSRC)/delayCmd.cpp


# DBDINC_SRCS should be last of the series of SOURCES
SOURCES += $(DBDINC_SRCS)

DBDS += $(APPSRC)/delayDo.dbd
DBDS += $(APPSRC)/stdSupport.dbd

# SCRIPTS += showBurtDiff wrapCmd wrapper

.PHONY: $(DBDINC_DEPS)
$(DBDINC_DEPS): $(DBDINC_HDRS)

.PHONY: .dbd.h
.dbd.h:
	$(DBTORECORDTYPEH)  $(USR_DBDFLAGS) -o  $@ $<



.PHONY: vlibs
vlibs:
#
